import { Component, OnInit } from '@angular/core';
import { Meta, MetaDefinition } from '@angular/platform-browser';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {

  constructor(private metaService: Meta) {
    this.updateMetaTags();
  }
  updateMetaTags() {
    this.metaService.updateTag({ name: 'description', content: 'contact-us page desc' }, "name='description'");
    this.metaService.updateTag({ property: 'og:title', content: 'contact-us page og title' }, "property='og:title'");
  }

  ngOnInit(): void {

  }

}
