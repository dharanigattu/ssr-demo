import { Component, OnInit } from '@angular/core';
import { Meta, MetaDefinition } from '@angular/platform-browser';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private metaService: Meta) {
    this.updateMetaTags();
  }
  updateMetaTags() {
    this.metaService.updateTag({ name: 'description', content: 'Home page desc' }, "name='description'");
    this.metaService.updateTag({ property: 'og:title', content: 'Home page og title' }, "property='og:title'");
  }

  ngOnInit(): void {

  }

}
